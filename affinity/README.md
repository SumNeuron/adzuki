# Affinity
Affinity refers to the application in which these files were made, i.e.
Affinity Designer.

These files are used to generate icon / logo files and other assets seen around
the site / package.  Thus the source files are those ending in `.afdesign`
