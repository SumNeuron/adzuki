//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var script = {
  name: 'Splinter',
  props: {
    text: {
      type: String,
      default: 'Default Text',
      validator: function validator(value) {
        return typeof value === 'string';
      }
    }
  },
  computed: {
    words: function words() {
      return this.text.split(' ');
    }
  },
  methods: {
    wordStyle: function wordStyle(index) {
      return {
        '--word-index': index
      };
    },
    charStyle: function charStyle(index, jndex) {
      var sty = this.wordStyle(index);
      sty['--char-index'] = jndex;
      sty['--abs-index'] = this.absolute_index(index, jndex);
      return sty;
    },
    chars: function chars(str) {
      return str.split('');
    },
    notLastNorOneQ: function notLastNorOneQ(wordIndex) {
      return wordIndex < this.words.length && this.words.length > 1;
    },
    absolute_index: function absolute_index(wordIndex, charIndex) {
      var that = this;
      var absIndex = 0;

      for (var i = 0; i < wordIndex; i++) {
        absIndex += that.words[i].length + (that.notLastNorOneQ(wordIndex) ? 1 : 0);
      }

      absIndex += charIndex;
      return absIndex;
    }
  }
};

/* script */
            const __vue_script__ = script;
            
/* template */
var __vue_render__ = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    { staticClass: "splinter" },
    _vm._l(_vm.words, function(word, index) {
      return _c(
        "span",
        { key: word.id, staticClass: "word", style: _vm.wordStyle(index) },
        [
          _vm._l(_vm.chars(word), function(char, jndex) {
            return _c(
              "span",
              {
                key: char.id,
                staticClass: "char",
                style: _vm.charStyle(index, jndex)
              },
              [_vm._v("\n      " + _vm._s(char) + "\n    ")]
            )
          }),
          _vm._v(" "),
          _vm.notLastNorOneQ(index)
            ? _c(
                "span",
                {
                  staticClass: "char space",
                  style: _vm.charStyle(index, _vm.chars(word).length + 1)
                },
                [_vm._v("\n       \n    ")]
              )
            : _vm._e()
        ],
        2
      )
    })
  )
};
var __vue_staticRenderFns__ = [];
__vue_render__._withStripped = true;

  /* style */
  const __vue_inject_styles__ = function (inject) {
    if (!inject) return
    inject("data-v-5fd6c7e1_0", { source: "\n.splinter {\n  display: inline-block;\n}\n.word {\n  display: inline-block;\n}\n.char {\n  display: inline-block\n}\n\n", map: {"version":3,"sources":["/Users/sumner/Programming/adzuki/src/components/Splinter/Splinter.vue"],"names":[],"mappings":";AAiFA;EACA,sBAAA;CACA;AAEA;EACA,sBAAA;CACA;AAEA;EACA,qBAAA;CACA","file":"Splinter.vue","sourcesContent":["<template>\n  <div class=\"splinter\">\n    <span\n      class=\"word\"\n      v-for=\"(word, index) in words\"\n      :style=\"wordStyle(index)\"\n      :key=\"word.id\"\n    >\n      <span\n        class=\"char\"\n        v-for=\"(char, jndex) in chars(word)\"\n        :style=\"charStyle(index, jndex)\"\n        :key=\"char.id\"\n      >\n        {{char}}\n      </span>\n\n      <span\n        class=\"char space\"\n        v-if=\"notLastNorOneQ(index)\"\n        :style=\"charStyle(index, chars(word).length + 1)\"\n\n      >\n        &nbsp;\n      </span>\n    </span>\n  </div>\n</template>\n\n<script>\nexport default {\n  name: 'Splinter',\n  props: {\n    text: {\n      type: String,\n      default: 'Default Text',\n      validator: function(value) {\n        return typeof value === 'string'\n      }\n    }\n  },\n  computed: {\n    words: function() {\n      return this.text.split(' ')\n    },\n\n  },\n  methods: {\n    wordStyle: function (index) {\n      return {\n        '--word-index': index\n      }\n    },\n\n    charStyle: function (index, jndex) {\n      var sty = this.wordStyle(index)\n      sty['--char-index'] = jndex;\n      sty['--abs-index'] = this.absolute_index(index, jndex)\n      return sty\n    },\n\n    chars: function(str) {\n      return str.split('')\n    },\n    notLastNorOneQ: function(wordIndex) {\n      return wordIndex < this.words.length && this.words.length > 1\n    },\n    absolute_index: function(wordIndex, charIndex) {\n      var that = this;\n      var absIndex = 0;\n      for (var i = 0; i < wordIndex; i++) {\n        absIndex += that.words[i].length + (that.notLastNorOneQ(wordIndex) ? 1 : 0)\n      }\n      absIndex += charIndex\n      return absIndex\n    }\n  }\n}\n</script>\n\n<style>\n.splinter {\n  display: inline-block;\n}\n\n.word {\n  display: inline-block;\n}\n\n.char {\n  display: inline-block\n}\n\n</style>\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__ = undefined;
  /* module identifier */
  const __vue_module_identifier__ = undefined;
  /* functional template */
  const __vue_is_functional_template__ = false;
  /* component normalizer */
  function __vue_normalize__(
    template, style, script$$1,
    scope, functional, moduleIdentifier,
    createInjector, createInjectorSSR
  ) {
    const component = (typeof script$$1 === 'function' ? script$$1.options : script$$1) || {};

    // For security concerns, we use only base name in production mode.
    component.__file = "/Users/sumner/Programming/adzuki/src/components/Splinter/Splinter.vue";

    if (!component.render) {
      component.render = template.render;
      component.staticRenderFns = template.staticRenderFns;
      component._compiled = true;

      if (functional) component.functional = true;
    }

    component._scopeId = scope;

    {
      let hook;
      if (style) {
        hook = function(context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook !== undefined) {
        if (component.functional) {
          // register for functional component in vue file
          const originalRender = component.render;
          component.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context)
          };
        } else {
          // inject component registration as beforeCreate hook
          const existing = component.beforeCreate;
          component.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }
    }

    return component
  }
  /* style inject */
  function __vue_create_injector__() {
    const head = document.head || document.getElementsByTagName('head')[0];
    const styles = __vue_create_injector__.styles || (__vue_create_injector__.styles = {});
    const isOldIE =
      typeof navigator !== 'undefined' &&
      /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

    return function addStyle(id, css) {
      if (document.querySelector('style[data-vue-ssr-id~="' + id + '"]')) return // SSR styles are present.

      const group = isOldIE ? css.media || 'default' : id;
      const style = styles[group] || (styles[group] = { ids: [], parts: [], element: undefined });

      if (!style.ids.includes(id)) {
        let code = css.source;
        let index = style.ids.length;

        style.ids.push(id);

        if (isOldIE) {
          style.element = style.element || document.querySelector('style[data-group=' + group + ']');
        }

        if (!style.element) {
          const el = style.element = document.createElement('style');
          el.type = 'text/css';

          if (css.media) el.setAttribute('media', css.media);
          if (isOldIE) {
            el.setAttribute('data-group', group);
            el.setAttribute('data-next-index', '0');
          }

          head.appendChild(el);
        }

        if (isOldIE) {
          index = parseInt(style.element.getAttribute('data-next-index'));
          style.element.setAttribute('data-next-index', index + 1);
        }

        if (style.element.styleSheet) {
          style.parts.push(code);
          style.element.styleSheet.cssText = style.parts
            .filter(Boolean)
            .join('\n');
        } else {
          const textNode = document.createTextNode(code);
          const nodes = style.element.childNodes;
          if (nodes[index]) style.element.removeChild(nodes[index]);
          if (nodes.length) style.element.insertBefore(textNode, nodes[index]);
          else style.element.appendChild(textNode);
        }
      }
    }
  }
  /* style inject SSR */
  

  
  var Splinter = __vue_normalize__(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    __vue_create_injector__,
    undefined
  );

//
//
//
//
//
//
var script$1 = {
  name: 'ButtonClose',
  methods: {
    clicked: function clicked() {
      this.$emit('close');
    }
  }
};

/* script */
            const __vue_script__$1 = script$1;
            
/* template */
var __vue_render__$1 = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "button",
    {
      staticClass: "close",
      attrs: { type: "button" },
      on: { click: _vm.clicked }
    },
    [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
  )
};
var __vue_staticRenderFns__$1 = [];
__vue_render__$1._withStripped = true;

  /* style */
  const __vue_inject_styles__$1 = function (inject) {
    if (!inject) return
    inject("data-v-d842ba4e_0", { source: "\nbutton.close[data-v-d842ba4e] {\n  float: none;\n}\n", map: {"version":3,"sources":["/Users/sumner/Programming/adzuki/src/components/ButtonClose/ButtonClose.vue"],"names":[],"mappings":";AAkBA;EACA,YAAA;CACA","file":"ButtonClose.vue","sourcesContent":["<template>\n  <button type=\"button\" class=\"close\" @click='clicked'>\n    <span aria-hidden=\"true\">&times;</span>\n  </button>\n</template>\n\n<script>\nexport default {\n  name: 'ButtonClose',\n  methods: {\n    clicked() {\n      this.$emit('close')\n    }\n  }\n}\n</script>\n\n<style scoped>\nbutton.close {\n  float: none;\n}\n</style>\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$1 = "data-v-d842ba4e";
  /* module identifier */
  const __vue_module_identifier__$1 = undefined;
  /* functional template */
  const __vue_is_functional_template__$1 = false;
  /* component normalizer */
  function __vue_normalize__$1(
    template, style, script,
    scope, functional, moduleIdentifier,
    createInjector, createInjectorSSR
  ) {
    const component = (typeof script === 'function' ? script.options : script) || {};

    // For security concerns, we use only base name in production mode.
    component.__file = "/Users/sumner/Programming/adzuki/src/components/ButtonClose/ButtonClose.vue";

    if (!component.render) {
      component.render = template.render;
      component.staticRenderFns = template.staticRenderFns;
      component._compiled = true;

      if (functional) component.functional = true;
    }

    component._scopeId = scope;

    {
      let hook;
      if (style) {
        hook = function(context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook !== undefined) {
        if (component.functional) {
          // register for functional component in vue file
          const originalRender = component.render;
          component.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context)
          };
        } else {
          // inject component registration as beforeCreate hook
          const existing = component.beforeCreate;
          component.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }
    }

    return component
  }
  /* style inject */
  function __vue_create_injector__$1() {
    const head = document.head || document.getElementsByTagName('head')[0];
    const styles = __vue_create_injector__$1.styles || (__vue_create_injector__$1.styles = {});
    const isOldIE =
      typeof navigator !== 'undefined' &&
      /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

    return function addStyle(id, css) {
      if (document.querySelector('style[data-vue-ssr-id~="' + id + '"]')) return // SSR styles are present.

      const group = isOldIE ? css.media || 'default' : id;
      const style = styles[group] || (styles[group] = { ids: [], parts: [], element: undefined });

      if (!style.ids.includes(id)) {
        let code = css.source;
        let index = style.ids.length;

        style.ids.push(id);

        if (isOldIE) {
          style.element = style.element || document.querySelector('style[data-group=' + group + ']');
        }

        if (!style.element) {
          const el = style.element = document.createElement('style');
          el.type = 'text/css';

          if (css.media) el.setAttribute('media', css.media);
          if (isOldIE) {
            el.setAttribute('data-group', group);
            el.setAttribute('data-next-index', '0');
          }

          head.appendChild(el);
        }

        if (isOldIE) {
          index = parseInt(style.element.getAttribute('data-next-index'));
          style.element.setAttribute('data-next-index', index + 1);
        }

        if (style.element.styleSheet) {
          style.parts.push(code);
          style.element.styleSheet.cssText = style.parts
            .filter(Boolean)
            .join('\n');
        } else {
          const textNode = document.createTextNode(code);
          const nodes = style.element.childNodes;
          if (nodes[index]) style.element.removeChild(nodes[index]);
          if (nodes.length) style.element.insertBefore(textNode, nodes[index]);
          else style.element.appendChild(textNode);
        }
      }
    }
  }
  /* style inject SSR */
  

  
  var ButtonClose = __vue_normalize__$1(
    { render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 },
    __vue_inject_styles__$1,
    __vue_script__$1,
    __vue_scope_id__$1,
    __vue_is_functional_template__$1,
    __vue_module_identifier__$1,
    __vue_create_injector__$1,
    undefined
  );

//
var script$2 = {
  components: {
    ButtonClose: ButtonClose
  },
  props: {
    type: {
      type: String,
      default: 'info'
    },
    title: {
      type: String,
      default: 'Info'
    },
    message: {
      type: String,
      default: 'this is a default message'
    }
  },
  methods: {
    clicked: function clicked() {
      this.$emit('close');
    }
  }
};

/* script */
            const __vue_script__$2 = script$2;
            
/* template */
var __vue_render__$2 = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    {
      staticClass: "alert d-flex justify-content-between",
      class: "alert-" + _vm.type
    },
    [
      _c("span", [
        _c("strong", [_vm._v(_vm._s(_vm.title))]),
        _vm._v(_vm._s(_vm.message))
      ]),
      _vm._v(" "),
      _c("ButtonClose", { on: { close: _vm.clicked } })
    ],
    1
  )
};
var __vue_staticRenderFns__$2 = [];
__vue_render__$2._withStripped = true;

  /* style */
  const __vue_inject_styles__$2 = function (inject) {
    if (!inject) return
    inject("data-v-15bb684e_0", { source: "\nstrong[data-v-15bb684e]:after {\n  content: ': '\n}\n", map: {"version":3,"sources":["/Users/sumner/Programming/adzuki/src/components/Alert/Alert.vue"],"names":[],"mappings":";AAuCA;EACA,aAAA;CACA","file":"Alert.vue","sourcesContent":["<template>\n  <div\n    class=\"alert d-flex justify-content-between\"\n    :class=\"'alert-'+type\"\n  >\n    <span><strong>{{title}}</strong>{{message}}</span>\n    <ButtonClose @close='clicked'/>\n  </div>\n</template>\n\n<script>\nimport ButtonClose from '../ButtonClose/ButtonClose.vue';\nexport default {\n  components: {\n    ButtonClose\n  },\n  props: {\n    type: {\n      type: String,\n      default: 'info'\n    },\n    title: {\n      type: String,\n      default: 'Info'\n    },\n    message: {\n      type: String,\n      default: 'this is a default message'\n    }\n  },\n  methods: {\n    clicked() {\n      this.$emit('close')\n    }\n  }\n}\n</script>\n\n<style scoped>\nstrong:after {\n  content: ': '\n}\n</style>\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$2 = "data-v-15bb684e";
  /* module identifier */
  const __vue_module_identifier__$2 = undefined;
  /* functional template */
  const __vue_is_functional_template__$2 = false;
  /* component normalizer */
  function __vue_normalize__$2(
    template, style, script,
    scope, functional, moduleIdentifier,
    createInjector, createInjectorSSR
  ) {
    const component = (typeof script === 'function' ? script.options : script) || {};

    // For security concerns, we use only base name in production mode.
    component.__file = "/Users/sumner/Programming/adzuki/src/components/Alert/Alert.vue";

    if (!component.render) {
      component.render = template.render;
      component.staticRenderFns = template.staticRenderFns;
      component._compiled = true;

      if (functional) component.functional = true;
    }

    component._scopeId = scope;

    {
      let hook;
      if (style) {
        hook = function(context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook !== undefined) {
        if (component.functional) {
          // register for functional component in vue file
          const originalRender = component.render;
          component.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context)
          };
        } else {
          // inject component registration as beforeCreate hook
          const existing = component.beforeCreate;
          component.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }
    }

    return component
  }
  /* style inject */
  function __vue_create_injector__$2() {
    const head = document.head || document.getElementsByTagName('head')[0];
    const styles = __vue_create_injector__$2.styles || (__vue_create_injector__$2.styles = {});
    const isOldIE =
      typeof navigator !== 'undefined' &&
      /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

    return function addStyle(id, css) {
      if (document.querySelector('style[data-vue-ssr-id~="' + id + '"]')) return // SSR styles are present.

      const group = isOldIE ? css.media || 'default' : id;
      const style = styles[group] || (styles[group] = { ids: [], parts: [], element: undefined });

      if (!style.ids.includes(id)) {
        let code = css.source;
        let index = style.ids.length;

        style.ids.push(id);

        if (isOldIE) {
          style.element = style.element || document.querySelector('style[data-group=' + group + ']');
        }

        if (!style.element) {
          const el = style.element = document.createElement('style');
          el.type = 'text/css';

          if (css.media) el.setAttribute('media', css.media);
          if (isOldIE) {
            el.setAttribute('data-group', group);
            el.setAttribute('data-next-index', '0');
          }

          head.appendChild(el);
        }

        if (isOldIE) {
          index = parseInt(style.element.getAttribute('data-next-index'));
          style.element.setAttribute('data-next-index', index + 1);
        }

        if (style.element.styleSheet) {
          style.parts.push(code);
          style.element.styleSheet.cssText = style.parts
            .filter(Boolean)
            .join('\n');
        } else {
          const textNode = document.createTextNode(code);
          const nodes = style.element.childNodes;
          if (nodes[index]) style.element.removeChild(nodes[index]);
          if (nodes.length) style.element.insertBefore(textNode, nodes[index]);
          else style.element.appendChild(textNode);
        }
      }
    }
  }
  /* style inject SSR */
  

  
  var Alert = __vue_normalize__$2(
    { render: __vue_render__$2, staticRenderFns: __vue_staticRenderFns__$2 },
    __vue_inject_styles__$2,
    __vue_script__$2,
    __vue_scope_id__$2,
    __vue_is_functional_template__$2,
    __vue_module_identifier__$2,
    __vue_create_injector__$2,
    undefined
  );

//
var script$3 = {
  name: 'AlertCenterItem',
  components: {
    Alert: Alert
  },
  props: {
    collaspedQ: {
      type: Boolean,
      default: true
    },
    alert: {
      type: Object,
      default: function _default() {
        return {
          title: '',
          message: ''
        };
      }
    },
    index: {
      type: Number,
      default: 0
    },
    verticalShift: {
      type: Number,
      default: 15
    },
    hoverShift: {
      type: Number,
      default: 15
    },
    hoverOpacityScale: {
      type: Number,
      default: 1
    },
    totalElements: {
      type: Number,
      default: 1
    }
  },
  data: function data() {
    return {
      hoveredQ: false
    };
  },
  computed: {
    top: function top() {
      var hoverShift = this.hoveredQ ? this.hoverShift : 0;
      return this.index * this.verticalShift - hoverShift;
    },
    marginTop: function marginTop() {
      return -this.verticalShift;
    },
    opacity: function opacity() {
      var perc = (this.index + 1) / this.totalElements;
      var opac = 1 - (1 - perc) * this.hoverOpacityScale;
      return this.collaspedQ ? opac : 1;
    },
    boxShadow: function boxShadow() {
      return "0px ".concat(this.top / 5, "px 15px 0px");
    },
    style: function style() {
      var sty = {
        marginTop: "".concat(this.marginTop, "px"),
        // top: `${this.top}px`,
        opacity: "".concat(this.opacity, " !important") // boxShadow: this.boxShadow

      };

      if (this.hoveredQ && this.collaspedQ) {
        sty.transform = "translateY(-".concat(this.hoverShift, "px)");
      }

      return sty;
    }
  },
  methods: {
    clicked: function clicked() {
      this.$emit('close', this.index);
    }
  }
};

/* script */
            const __vue_script__$3 = script$3;
            
/* template */
var __vue_render__$3 = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("Alert", {
    staticClass: "alert-center-item",
    style: _vm.style,
    attrs: {
      title: _vm.alert.title,
      message: _vm.alert.message,
      type: _vm.alert.type
    },
    on: { close: _vm.clicked },
    nativeOn: {
      mouseover: function($event) {
        _vm.hoveredQ = true;
      },
      mouseleave: function($event) {
        _vm.hoveredQ = false;
      }
    }
  })
};
var __vue_staticRenderFns__$3 = [];
__vue_render__$3._withStripped = true;

  /* style */
  const __vue_inject_styles__$3 = function (inject) {
    if (!inject) return
    inject("data-v-680318d0_0", { source: "\n.alert-center-item[data-v-680318d0] {\n  transition: all 0.5s ease;\n  /* position: absolute; */\n  width: 100%;\n  text-align: left;\n}\n\n", map: {"version":3,"sources":["/Users/sumner/Programming/adzuki/src/components/AlertCenterItem/AlertCenterItem.vue"],"names":[],"mappings":";AAyFA;EACA,0BAAA;EACA,yBAAA;EACA,YAAA;EACA,iBAAA;CACA","file":"AlertCenterItem.vue","sourcesContent":["<template>\n    <Alert\n      class=\"alert-center-item\"\n      :style='style'\n      :title='alert.title'\n      :message='alert.message'\n      :type='alert.type'\n      @mouseover.native=\"hoveredQ=true\"\n      @mouseleave.native=\"hoveredQ=false\"\n      @close=\"clicked\"\n    />\n</template>\n\n<script>\nimport Alert from '../Alert/Alert.vue';\n\nexport default {\n  name: 'AlertCenterItem',\n  components: {Alert},\n  props: {\n    collaspedQ: {\n      type: Boolean,\n      default: true\n    },\n    alert: {\n      type: Object,\n      default: () => {return {title: '', message: ''}}\n    },\n    index: {\n      type: Number,\n      default: 0\n    },\n    verticalShift: {\n      type: Number, default: 15\n    },\n    hoverShift: {\n      type: Number, default: 15\n    },\n    hoverOpacityScale: {\n      type:Number, default: 1\n    },\n    totalElements: {\n      type:Number, default: 1\n    }\n  },\n  data() {\n    return {\n      hoveredQ: false\n    }\n  },\n  computed: {\n    top() {\n      var hoverShift = (this.hoveredQ ? this.hoverShift: 0)\n      return this.index * this.verticalShift - hoverShift\n    },\n    marginTop() {\n      return -this.verticalShift\n    },\n    opacity() {\n      var perc = (this.index + 1) / this.totalElements\n      var opac = 1 - (1 - perc) * this.hoverOpacityScale\n      return this.collaspedQ ? opac : 1\n    },\n    boxShadow() {\n      return `0px ${this.top / 5}px 15px 0px`\n    },\n    style() {\n      var sty = {\n        marginTop: `${this.marginTop}px`,\n        // top: `${this.top}px`,\n        opacity: `${this.opacity} !important`,\n        // boxShadow: this.boxShadow\n      };\n      if (this.hoveredQ && this.collaspedQ) {\n        sty.transform = `translateY(-${this.hoverShift}px)`;\n      }\n      return sty\n    }\n  },\n  methods: {\n    clicked() {\n      this.$emit('close', this.index)\n    }\n  }\n\n}\n</script>\n\n<style scoped>\n.alert-center-item {\n  transition: all 0.5s ease;\n  /* position: absolute; */\n  width: 100%;\n  text-align: left;\n}\n\n</style>\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$3 = "data-v-680318d0";
  /* module identifier */
  const __vue_module_identifier__$3 = undefined;
  /* functional template */
  const __vue_is_functional_template__$3 = false;
  /* component normalizer */
  function __vue_normalize__$3(
    template, style, script,
    scope, functional, moduleIdentifier,
    createInjector, createInjectorSSR
  ) {
    const component = (typeof script === 'function' ? script.options : script) || {};

    // For security concerns, we use only base name in production mode.
    component.__file = "/Users/sumner/Programming/adzuki/src/components/AlertCenterItem/AlertCenterItem.vue";

    if (!component.render) {
      component.render = template.render;
      component.staticRenderFns = template.staticRenderFns;
      component._compiled = true;

      if (functional) component.functional = true;
    }

    component._scopeId = scope;

    {
      let hook;
      if (style) {
        hook = function(context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook !== undefined) {
        if (component.functional) {
          // register for functional component in vue file
          const originalRender = component.render;
          component.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context)
          };
        } else {
          // inject component registration as beforeCreate hook
          const existing = component.beforeCreate;
          component.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }
    }

    return component
  }
  /* style inject */
  function __vue_create_injector__$3() {
    const head = document.head || document.getElementsByTagName('head')[0];
    const styles = __vue_create_injector__$3.styles || (__vue_create_injector__$3.styles = {});
    const isOldIE =
      typeof navigator !== 'undefined' &&
      /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

    return function addStyle(id, css) {
      if (document.querySelector('style[data-vue-ssr-id~="' + id + '"]')) return // SSR styles are present.

      const group = isOldIE ? css.media || 'default' : id;
      const style = styles[group] || (styles[group] = { ids: [], parts: [], element: undefined });

      if (!style.ids.includes(id)) {
        let code = css.source;
        let index = style.ids.length;

        style.ids.push(id);

        if (isOldIE) {
          style.element = style.element || document.querySelector('style[data-group=' + group + ']');
        }

        if (!style.element) {
          const el = style.element = document.createElement('style');
          el.type = 'text/css';

          if (css.media) el.setAttribute('media', css.media);
          if (isOldIE) {
            el.setAttribute('data-group', group);
            el.setAttribute('data-next-index', '0');
          }

          head.appendChild(el);
        }

        if (isOldIE) {
          index = parseInt(style.element.getAttribute('data-next-index'));
          style.element.setAttribute('data-next-index', index + 1);
        }

        if (style.element.styleSheet) {
          style.parts.push(code);
          style.element.styleSheet.cssText = style.parts
            .filter(Boolean)
            .join('\n');
        } else {
          const textNode = document.createTextNode(code);
          const nodes = style.element.childNodes;
          if (nodes[index]) style.element.removeChild(nodes[index]);
          if (nodes.length) style.element.insertBefore(textNode, nodes[index]);
          else style.element.appendChild(textNode);
        }
      }
    }
  }
  /* style inject SSR */
  

  
  var AlertCenterItem = __vue_normalize__$3(
    { render: __vue_render__$3, staticRenderFns: __vue_staticRenderFns__$3 },
    __vue_inject_styles__$3,
    __vue_script__$3,
    __vue_scope_id__$3,
    __vue_is_functional_template__$3,
    __vue_module_identifier__$3,
    __vue_create_injector__$3,
    undefined
  );

//
var script$4 = {
  name: 'AlertCenter',
  components: {
    AlertCenterItem: AlertCenterItem
  },
  props: {
    alerts: {
      type: Array,
      default: function _default() {
        return [{
          title: 'title',
          message: 'message 1',
          type: 'info'
        }, {
          title: 'title',
          message: 'message 2',
          type: 'info'
        }, {
          title: 'title',
          message: 'message 3',
          type: 'info'
        }, {
          title: 'title',
          message: 'message 4',
          type: 'info'
        }, {
          title: 'title',
          message: 'message 5',
          type: 'info'
        }, {
          title: 'title',
          message: 'message 6',
          type: 'info'
        }];
      }
    },
    verticalShift: {
      type: Number,
      default: 55
    },
    hoverShift: {
      type: Number,
      default: 15
    }
  },
  data: function data() {
    return {
      shift: this.verticalShift,
      pad: this.verticalShift,
      collapsedQ: true
    };
  },
  methods: {
    remove: function remove(index) {
      this.alerts.splice(index, 1);
    },
    toggleCollapse: function toggleCollapse() {
      this.collapsedQ = !this.collapsedQ;
      this.shift = this.collapsedQ ? this.verticalShift : 0;
      this.pad = this.collapsedQ ? this.verticalShift : 0;
    }
  },
  computed: {
    style: function style() {
      return {
        paddingTop: "".concat(this.pad, "px"),
        position: 'relative'
      };
    }
  }
};

/* script */
            const __vue_script__$4 = script$4;
            
/* template */
var __vue_render__$4 = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c("div", [
    _c("link", {
      attrs: {
        rel: "stylesheet",
        href:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css",
        integrity:
          "sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO",
        crossorigin: "anonymous"
      }
    }),
    _vm._v(" "),
    _c(
      "ul",
      { staticClass: "mx-auto", style: _vm.style },
      _vm._l(_vm.alerts, function(alert, index) {
        return _c("AlertCenterItem", {
          key: alert.id,
          attrs: {
            index: index,
            alert: alert,
            totalElements: _vm.alerts.length,
            verticalShift: _vm.shift,
            hoverShift: _vm.hoverShift,
            collaspedQ: _vm.collapsedQ
          },
          on: {
            close: function($event) {
              _vm.remove(index);
            }
          }
        })
      })
    ),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "btn",
        on: {
          click: function($event) {
            _vm.toggleCollapse();
          }
        }
      },
      [_vm._v("\n    click to collapse / expand\n  ")]
    )
  ])
};
var __vue_staticRenderFns__$4 = [];
__vue_render__$4._withStripped = true;

  /* style */
  const __vue_inject_styles__$4 = function (inject) {
    if (!inject) return
    inject("data-v-53b201cd_0", { source: "\nul {\n  padding: 0px;\n  margin: 0px;\n}\n", map: {"version":3,"sources":["/Users/sumner/Programming/adzuki/src/components/AlertCenter/AlertCenter.vue"],"names":[],"mappings":";AAgFA;EACA,aAAA;EACA,YAAA;CACA","file":"AlertCenter.vue","sourcesContent":["<template>\n  <div>\n    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">\n\n    <ul class=\"mx-auto\" :style=\"style\">\n      <AlertCenterItem\n        v-for=\"(alert, index) in alerts\"\n        :key=\"alert.id\"\n        :index=\"index\"\n        :alert=\"alert\"\n        :totalElements=\"alerts.length\"\n        :verticalShift=\"shift\"\n        :hoverShift=\"hoverShift\"\n        :collaspedQ=\"collapsedQ\"\n        @close=\"remove(index)\"\n      />\n    </ul>\n    <button class=\"btn\" @click=\"toggleCollapse()\">\n      click to collapse / expand\n    </button>\n  </div>\n\n</template>\n\n<script>\nimport AlertCenterItem from '../AlertCenterItem/AlertCenterItem.vue';\n\nexport default {\n  name: 'AlertCenter',\n  components: {AlertCenterItem},\n  props: {\n    alerts: {\n      type: Array,\n      default: () => [\n        {title: 'title', message: 'message 1', type: 'info'},\n        {title: 'title', message: 'message 2', type: 'info'},\n        {title: 'title', message: 'message 3', type: 'info'},\n        {title: 'title', message: 'message 4', type: 'info'},\n        {title: 'title', message: 'message 5', type: 'info'},\n        {title: 'title', message: 'message 6', type: 'info'},\n      ]\n    },\n    verticalShift: {\n      type: Number,\n      default: 55\n    },\n    hoverShift: {\n      type: Number,\n      default: 15\n    }\n  },\n  data() {\n    return {\n      shift: this.verticalShift,\n      pad: this.verticalShift,\n      collapsedQ: true\n    }\n  },\n  methods: {\n    remove(index) {\n      this.alerts.splice(index, 1)\n    },\n    toggleCollapse() {\n      this.collapsedQ = !this.collapsedQ\n      this.shift = this.collapsedQ ? this.verticalShift : 0\n      this.pad = this.collapsedQ ? this.verticalShift : 0\n    }\n  },\n  computed: {\n    style() {\n      return {\n        paddingTop: `${this.pad}px`,\n        position: 'relative'\n      }\n    }\n  }\n}\n</script>\n\n<style>\nul {\n  padding: 0px;\n  margin: 0px;\n}\n</style>\n"]}, media: undefined });

  };
  /* scoped */
  const __vue_scope_id__$4 = undefined;
  /* module identifier */
  const __vue_module_identifier__$4 = undefined;
  /* functional template */
  const __vue_is_functional_template__$4 = false;
  /* component normalizer */
  function __vue_normalize__$4(
    template, style, script,
    scope, functional, moduleIdentifier,
    createInjector, createInjectorSSR
  ) {
    const component = (typeof script === 'function' ? script.options : script) || {};

    // For security concerns, we use only base name in production mode.
    component.__file = "/Users/sumner/Programming/adzuki/src/components/AlertCenter/AlertCenter.vue";

    if (!component.render) {
      component.render = template.render;
      component.staticRenderFns = template.staticRenderFns;
      component._compiled = true;

      if (functional) component.functional = true;
    }

    component._scopeId = scope;

    {
      let hook;
      if (style) {
        hook = function(context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook !== undefined) {
        if (component.functional) {
          // register for functional component in vue file
          const originalRender = component.render;
          component.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context)
          };
        } else {
          // inject component registration as beforeCreate hook
          const existing = component.beforeCreate;
          component.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }
    }

    return component
  }
  /* style inject */
  function __vue_create_injector__$4() {
    const head = document.head || document.getElementsByTagName('head')[0];
    const styles = __vue_create_injector__$4.styles || (__vue_create_injector__$4.styles = {});
    const isOldIE =
      typeof navigator !== 'undefined' &&
      /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());

    return function addStyle(id, css) {
      if (document.querySelector('style[data-vue-ssr-id~="' + id + '"]')) return // SSR styles are present.

      const group = isOldIE ? css.media || 'default' : id;
      const style = styles[group] || (styles[group] = { ids: [], parts: [], element: undefined });

      if (!style.ids.includes(id)) {
        let code = css.source;
        let index = style.ids.length;

        style.ids.push(id);

        if (isOldIE) {
          style.element = style.element || document.querySelector('style[data-group=' + group + ']');
        }

        if (!style.element) {
          const el = style.element = document.createElement('style');
          el.type = 'text/css';

          if (css.media) el.setAttribute('media', css.media);
          if (isOldIE) {
            el.setAttribute('data-group', group);
            el.setAttribute('data-next-index', '0');
          }

          head.appendChild(el);
        }

        if (isOldIE) {
          index = parseInt(style.element.getAttribute('data-next-index'));
          style.element.setAttribute('data-next-index', index + 1);
        }

        if (style.element.styleSheet) {
          style.parts.push(code);
          style.element.styleSheet.cssText = style.parts
            .filter(Boolean)
            .join('\n');
        } else {
          const textNode = document.createTextNode(code);
          const nodes = style.element.childNodes;
          if (nodes[index]) style.element.removeChild(nodes[index]);
          if (nodes.length) style.element.insertBefore(textNode, nodes[index]);
          else style.element.appendChild(textNode);
        }
      }
    }
  }
  /* style inject SSR */
  

  
  var AlertCenter = __vue_normalize__$4(
    { render: __vue_render__$4, staticRenderFns: __vue_staticRenderFns__$4 },
    __vue_inject_styles__$4,
    __vue_script__$4,
    __vue_scope_id__$4,
    __vue_is_functional_template__$4,
    __vue_module_identifier__$4,
    __vue_create_injector__$4,
    undefined
  );

// NOTE: This setup uses 'babel-plugin-wildcard' to gather all .vue files
var components = {
  Splinter: Splinter,
  AlertCenter: AlertCenter // install function executed by Vue.use()

};
function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.keys(components).forEach(function (componentName) {
    Vue.component(componentName, components[componentName]);
  });
} // Create module definition for Vue.use()

var plugin = {
  install: install
}; // To auto-install when vue is found

/* global window */

var GlobalVue = null;

if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}

if (GlobalVue) {
  GlobalVue.use(plugin);
} // To allow use as module (npm/webpack/etc.) export component
// also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
// export const RollupDemoDirective = component;

export default components;
export { install };
