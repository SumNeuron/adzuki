<div style="text-align:center">
<img src="https://gitlab.com/SumNeuron/adzuki/raw/master/affinity/logo.png" width="100" height="100"/>
<span>dzuki</span>
<img src="https://gitlab.com/SumNeuron/adzuki/raw/master/affinity/icon.png" width="20" height="20"/>
</div>


# Adzuki

Adzuki is a library of [Vue] components to add a bit of flavor to any website.


[Vue]: https://vuejs.org/
[Logo]: ./affinity/logo.png
[Icon]: ./affinity/icon.png
