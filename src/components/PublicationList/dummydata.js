const pu1 = {
  title: {
    text: "Dynamic Control Flow in Large-Scale Machine Learning",
    href: ""
  },
  authors: [
    { name: "Yuan Yu", href: "#" },
    { name: "Martin Abadi" },
    { name: "Paul Barham" },
    { name: "Eugene Brevdo" },
    { name: "Mike Burrows" },
    { name: "Andy Davis" },
    { name: "Jeff Dean" },
    { name: "Sanjay Ghemawat" },
    { name: "Tim Harley" },
    { name: "Peter Hawkins" },
    { name: "Michael Isard" },
    { name: "Manjunath Kudlur" },
    { name: "Rajat Monga" },
    { name: "Derek Murray" },
    { name: "Xiaoqiang Zheng" }
  ],
  abstract:
    "Many recent machine learning models rely on fine-grained dynamic control flow for training and inference. In particular, models based on recurrent neural networks and on reinforcement learning depend on recurrence relations, data-dependent conditional execution, and other features that call for dynamic control flow. These applications benefit from the ability to make rapid control-flow decisions across a set of computing devices in a distributed system. For performance, scalability, and expressiveness, a machine learning system must support dynamic control flow in distributed and heterogeneous environments. This paper presents a programmi...",
  booktitle: { text: "Proceedings of EuroSys", href: "" },
  year: 2018,
  pages: [75, 83],
  vols: 59
};

const pu2 = {
  title: {
    text: "A Computational Model for TensorFlow (An Introduction)",
    href: ""
  },
  authors: [
    { name: "Martin Abadi" },
    { name: "Michael Isard" },
    { name: "Derek G.Murray" }
  ],
  booktitle: {
    text:
      " 1st ACM SIGPLAN Workshop on Machine Learning and Programming Languages (MAPL 2017)"
  },
  year: 2017
};

const pu3 = {
  title: {
    text: "Incremental, iterative data processing with timely dataflow",
    href: "#"
  },
  authors: [
    { name: "Derek G. Murray" },
    { name: "Frank McSherry" },
    { name: "Michael Isard" },
    { name: "Rebecca Isaacs" },
    { name: "Paul Barham" },
    { name: "Martin Abadi" }
  ],
  booktitle: { text: "Communications of the ACM" },
  volume: 59,
  year: 2016,
  pages: [75, 83]
};

const pubs = [pu1, pu2, pu3];
export { pubs, pu1, pu2, pu3 };
