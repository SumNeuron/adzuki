import { shallowMount } from '@vue/test-utils'
import Splinter from './Splinter.vue'

describe('Splinter', () => {

  it('splits the passed "text" into words', () => {
    const text = 'some text'
    const wrapper = shallowMount(Splinter, {
      propsData: { text }
    })

    expect(wrapper.words[0]).toMatch('some')
    expect(wrapper.words[1]).toMatch('text')

  })
})
