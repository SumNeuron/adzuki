# Splitter

Inspired by the library [Splitting JS][Splitting JS] and the easy animation it provides as shown in this [Courstro][Splitting JS Tutorial] video.

<svg width=50px charset=utf-8 xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 65 108'>
<path fill='#f0e216' d='M50.3 19.6c10.5 16.2 17.6 30.5 11 49-6 16.7-25 33.7-50.4 38 16.3-22 31.5-35.6 39.4-87z'/>
<clipPath id='a'>
  <path d='M50.3 19.6c10.5 16.2 17.6 30.5 11 49-6 16.7-25 33.7-50.4 38 16.3-22 31.5-35.6 39.4-87z'/>
</clipPath>
<g clip-path='url(#a)'>
</svg>


The `Splitter` component takes just one `"prop"` called `text` and will split (into `<span>`s) the passed text into words, characters and spaces. In addition it provides the following css variables that might be useful for css styling:

```
  --word-index: index of the <span> of the word
  --char-index: index of the character within the given word
  --abs-index: index of the character across all words
```

as well as classes:

```
  word: <span> containing a word
  char: <span> containing a char
  space: <span> containing a space (also has 'char')
```

The solution to the dynamic CSS variables was figured out via this [post][Dynamic Css Vars].




[Splitting JS]: [https://splitting.js.org/]
[Splitting JS Tutorial]: https://www.youtube.com/watch?v=ySrbvSf0xiA
[Dynamic Css Vars]: https://stackoverflow.com/questions/20490704/combine-calc-with-attr-in-css
