// NOTE: This setup uses 'babel-plugin-wildcard' to gather all .vue files
// from the components directory into one object, keyed by PascalCased versions
// of their filenames. The same could be accomplished manually by importing
// individual components and constructing an object from those imports. The resulting
// object should have PascalCased keys that match the actual component 'name' attribute.

// Import vue components
// import * as components from './components';

import Splinter from './components/Splinter/index.js';
import AlertCenter from './components/AlertCenter/index.js';
import Publication from './components/Publication/index.js';
import PublicationList from './components/PublicationList/index.js';
var components = {Splinter, AlertCenter, Publication, PublicationList}



// install function executed by Vue.use()
export function install(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.keys(components).forEach((componentName) => {
    Vue.component(componentName, components[componentName]);
  });
}

// Create module definition for Vue.use()
const plugin = {
  install,
};

// To auto-install when vue is found
/* global window */
let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// To allow use as module (npm/webpack/etc.) export component
export default components;

// It's possible to expose named exports when writing components that can
// also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
// export const RollupDemoDirective = component;
